# Frontend Microservices - GFT Academy 2020

You can find in this repository a few approaches how to create frontend microservices

## Iframe

This approach uses iframes and communicate between them with use of postMessage

## Portals

This approach uses new Chrome feature - portals

## Single spa

This approach uses https://single-spa.js.org with version of SystemJS < 3.0

## Single spa - new

This approach uses https://single-spa.js.org with version of SystemJS > 3.0

## Custom elements

This approach uses custom elements, git submodules, ES6 imports. For communication it uses CustomEvents

### Contact details: 
Norbert Blandzi (norbert.blandzi@gft.com)



