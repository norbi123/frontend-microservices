import './gft-component/gft-component.js'
import './gft-event-component/gft-event-component.js'

const template = /*html*/`
    <gft-component></gft-component>
    <gft-event-component></gft-event-component>
`

class App extends HTMLElement {
    constructor () {
        super()
    }

    connectedCallback () {
        this.innerHTML = template;
    }
}

window.customElements.define('app-component', App);