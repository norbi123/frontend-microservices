import React, { Component } from 'react';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <button onClick={() => {
          window.parent.postMessage(
            {
              from: 'react', message: 'Message from react'
            },
            '*'
            )
        }}> Send message </button>
      </div>
    );
  }
}

export default App;
