import React from 'react';

class App extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            hasError: false
        }
    }

    componentDidCatch () {
        this.setState({
            hasError: true
        });
    }

    render () {
        return (
            <div>React application</div>
        );
    }
}

export default App;