import React from 'react';
import ReactDOM from 'react-dom';
import singleSpaReact from 'single-spa-react';
import App from './App'

const lifecycles = singleSpaReact({
    React,
    ReactDOM,
    rootComponent: App,
    domElementGetter
})

export const bootstrap = [
    lifecycles.bootstrap
]

export const mount = [
    lifecycles.mount
]

export const unmount = [
    lifecycles.unmount
]

function domElementGetter () {
    return document.getElementById('app1');
}

if (location.port === "3000") {
    ReactDOM.render(<App />, document.getElementById('app1'))
}