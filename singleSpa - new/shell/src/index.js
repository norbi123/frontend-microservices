import * as singleSpa from 'single-spa';

singleSpa.registerApplication(
    'app1',
    async () => (await SystemJS.import('app1')).default,
        () => window.location.href.indexOf('app1') !== -1
);

singleSpa.start()