const path = require('path');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const HTMLWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: './src/index.js',
    output: {
        filename: 'index.js',
        path: path.resolve(__dirname, 'build/')
    },
    mode: 'development',
    module: {
        rules: [{
            test: /\.css$/,
            exclude: '/node_modules/',
            loader: ['style-loader', 'css-loader']
        }]
    },
    devServer: {
        port: 8000,
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HTMLWebpackPlugin({
            template: path.resolve(__dirname, 'index.html'),
            inject: 'body'
        })
    ]
}