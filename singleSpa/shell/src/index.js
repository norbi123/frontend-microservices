import SystemJS from 'systemjs';
import * as singleSpa from 'single-spa';
import "./styles.css";

const sofeManifest = {
    sofe: {
        manifest: {
            app1: 'http://localhost:3000/index.js'
        }
    }
};

SystemJS.config(sofeManifest)
registerDep('sofe', () => require('sofe'))

//register apps
singleSpa.registerApplication(
    'app1',
    () => SystemJS.import('app1!sofe'),
    () => window.location.href.indexOf('app1') !== -1
);

// A "requirer" is a function that requires a module. It is not called until
// a sofe service needs the dependency. This prevents the code from being executed
// unnecessarily during the critical initialization phase of the app
function registerDep(name, requirer) {
    SystemJS.registerDynamic(name, [], false, function(_r, _e, _m) {
      _m.exports = requirer()
    })
  }
  

singleSpa.start()