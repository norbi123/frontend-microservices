const path = require('path');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
    entry: "./src/index.js",
    output: {
        filename: 'index.js',
        path: path.resolve(__dirname, 'build')
    },
    mode: 'development',
    node: {
        fs: 'empty'
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            }]
    },
    resolve: {
        modules: [__dirname, 'node_modules'],
        alias: {
            systemjs: path.resolve(__dirname, 'node_modules/systemjs/dist/system.js')
        }
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'index.html'),
            inject: 'body'
        }),
        new CopyWebpackPlugin([{
            from: path.resolve(__dirname, 'index.html')
        }])
    ]
}